package br.com.ninjastestapp.home.view.impl;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.application.NinjasApplication;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.home.component.DaggerHomeComponent;
import br.com.ninjastestapp.home.presenter.HomePresenter;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.home.view.HomeView;
import br.com.ninjastestapp.home.view.adapter.HomeContainerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainScreenActivity extends AppCompatActivity implements HomeView {

    @BindView(R.id.orders_viewpager)
    ViewPager viewPager;

    @BindView(R.id.orders_tabs)
    TabLayout tabLayout;

    @Inject
    HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        setupInject();
        setupButterKnife();
        setTitle(getString(R.string.scren_title));
        homePresenter.setView(this);
        homePresenter.onViewCreated(savedInstanceState);

    }

    private void setupInject() {
        DaggerHomeComponent.builder()
                .networkComponent(((NinjasApplication) getApplication()).getNetworkInjection())
                .appModule(new AppModule((NinjasApplication) getApplication()))
                .build()
                .inject(this);
    }

    private void setupButterKnife() {
        ButterKnife.bind(this);
    }

    @Override
    public void setupViewPager() {
        final HomeContainerAdapter containerAdapter = new HomeContainerAdapter(
                getSupportFragmentManager()
        );

        containerAdapter.addPage(getString(R.string.str_offers_tab),
                Fragment.instantiate(getApplicationContext(),
                Constants.OFFER_FRAGMENT));

        containerAdapter.addPage(getString(R.string.str_lead_tab),
                Fragment.instantiate(getApplicationContext(),
                Constants.LEAD_FRAGMENT));

        this.tabLayout.setupWithViewPager(this.viewPager);
        this.viewPager.setAdapter(containerAdapter);
    }

    @Override
    public void showUrlRequestErrorMsg() {
        Toast.makeText(getApplicationContext(),
                getString(R.string.connection_error_msg),
                Toast.LENGTH_LONG).show();
    }
}
