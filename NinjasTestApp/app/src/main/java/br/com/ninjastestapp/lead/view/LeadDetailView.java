package br.com.ninjastestapp.lead.view;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.view.CoreView;

public interface LeadDetailView extends CoreView {
    void fillDetailScreen(DetailResponse detailResponse);

    void fillLeadInfo(String label, String value);

    void showDetailRequestError();

    void hideProgressBar();
}
