package br.com.ninjastestapp.core.networking;

import android.support.annotation.VisibleForTesting;

import okhttp3.OkHttpClient;

public class ApiCaller {

    private static OkHttpClient.Builder client;

    private ApiCaller() {
        // No instances!
    }

    public static OkHttpClient.Builder getClient() {
        if (client == null) {
            client = new OkHttpClient.Builder();
        }

        return client;
    }

    @VisibleForTesting
    public static void setClient(OkHttpClient.Builder client) {
        ApiCaller.client = client;
    }
}
