package br.com.ninjastestapp.core.model;

import com.google.gson.annotations.SerializedName;

public class RootLinks {

    @SerializedName("self")
    private Link url;

    public Link getUrl() {
        return url;
    }
}
