package br.com.ninjastestapp.offer.repository;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.offer.model.OffersResponse;
import rx.Observable;

public interface OffersRepository {

    Observable<OffersResponse> getOffers();

    Observable<DetailResponse> getOfferDetail(String url);

    Observable<OffersResponse> updateOffers(String offerUrl);
}
