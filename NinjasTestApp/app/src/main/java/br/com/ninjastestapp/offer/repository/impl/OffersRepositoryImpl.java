package br.com.ninjastestapp.offer.repository.impl;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.networking.OfferAPIService;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OffersRepositoryImpl implements OffersRepository {

    Retrofit retrofit;

    Context context;

    @Inject
    public OffersRepositoryImpl(Retrofit retrofit, Context context) {
        this.retrofit = retrofit;
        this.context = context;
    }

    @Override
    public Observable<OffersResponse> getOffers() {
        return updateOffers(retrieveOfferUrl());
    }

    @Override
    public Observable<DetailResponse> getOfferDetail(final String url) {
        return retrofit.create(OfferAPIService.class).getOfferDetail(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<OffersResponse> updateOffers(String selfLink) {
        return retrofit.create(OfferAPIService.class).getOffers(selfLink)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private String retrieveOfferUrl() {
        SharedPreferences pref = context.getSharedPreferences(
                Constants.URL_PREF_NAME, Context.MODE_PRIVATE
        );

        return pref.getString(Constants.OFFER_URL_KEY, null);
    }

}
