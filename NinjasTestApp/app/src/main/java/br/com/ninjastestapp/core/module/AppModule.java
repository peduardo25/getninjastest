package br.com.ninjastestapp.core.module;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import br.com.ninjastestapp.core.application.NinjasApplication;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private NinjasApplication ninjasApplication;

    public AppModule(final NinjasApplication chargebackApplication) {
        this.ninjasApplication = chargebackApplication;
    }

    @Provides
    @Singleton
    NinjasApplication chargeBackApplicationProvider() {
        return this.ninjasApplication;
    }

    @Provides
    Context contextProvider() {
        return this.ninjasApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources resourcesProvider() {
        return this.ninjasApplication.getResources();
    }
}
