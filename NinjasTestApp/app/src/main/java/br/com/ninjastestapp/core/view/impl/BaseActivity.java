package br.com.ninjastestapp.core.view.impl;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Geolocation;
import br.com.ninjastestapp.core.model.User;
import br.com.ninjastestapp.core.utils.NinjasUtil;
import butterknife.BindView;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.detail_toolbar)
    Toolbar toolbar;

    @BindView(R.id.detail_title)
    TextView detailTitle;

    @BindView(R.id.detail_name)
    TextView detailName;

    @BindView(R.id.detail_place)
    TextView detailPlace;

    @BindView(R.id.detail_distance)
    TextView detailDistance;

    @BindView(R.id.costumer_phone)
    TextView costumerPhone;

    @BindView(R.id.costumer_email)
    TextView costumerEmail;

    @BindView(R.id.contact_msg)
    TextView costumerContactMsg;

    @BindView(R.id.info_container)
    LinearLayout infoContainer;

    @BindView(R.id.costumer_detail_container)
    LinearLayout costumerDetailContainer;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    NinjasUtil ninjasUtil;

    private GoogleMap googleMap;


    public void setupActionBar(final String title) {
        setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void setupMap(Geolocation geolocation) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(map ->{
            googleMap = map;
            googleMap.getUiSettings().setZoomControlsEnabled(true);

            LatLng latLng = new LatLng(geolocation.getLatitude(), geolocation.getLongitude());

            animateToPosition(latLng);
            createMarker(latLng);
        } );
    }

    private void animateToPosition(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(17)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void createMarker(LatLng latLng){
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                .draggable(false).visible(true));
    }

    public void fillCostumerContact(User user, int backgroundColor, String contactMsg) {
        costumerPhone.setText(ninjasUtil.concatPhones(user.getEmbeddedResource().getPhones()));
        costumerEmail.setText(user.getEmail());
        costumerContactMsg.setText(contactMsg);
        costumerDetailContainer.setBackgroundColor(backgroundColor);
    }

    public void fillInfo(final String label, final String value){
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.info_container, null);
        TextView tvLabel = (TextView) view.findViewById(R.id.info_label);
        TextView tvValue = (TextView) view.findViewById(R.id.info_value);
        View v = view.findViewById(R.id.info_layout_container);
        tvLabel.setText(label);
        tvValue.setText(value);
        infoContainer.addView(v);
    }

    public void fillHeaderData(DetailResponse offer) {
        detailTitle.setText(offer.getTitle());
        detailName.setText(offer.getEmbeddedResource().getUser().getName());

        String place = String.format(getString(R.string.place_label),
                offer.getEmbeddedResource().getAddress().getNeighborhood(),
                offer.getEmbeddedResource().getAddress().getUf());

        detailPlace.setText(place);
        String distance = String.format(getString(R.string.place_distance),
                ninjasUtil.calcDistanceInKm(offer.getDistance()));
        detailDistance.setText(distance);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
