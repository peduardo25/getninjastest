package br.com.ninjastestapp.offer.presenter;

import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.view.OffersView;

public interface OffersPresenter extends AbstractPresenter<OffersView> {
    void onViewCreated();

    void updateList(OffersResponse offersResponse);
}
