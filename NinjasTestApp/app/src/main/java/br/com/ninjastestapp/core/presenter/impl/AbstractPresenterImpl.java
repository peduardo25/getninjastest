package br.com.ninjastestapp.core.presenter.impl;

import android.os.Bundle;

import br.com.ninjastestapp.core.view.CoreView;

public class AbstractPresenterImpl<T extends CoreView> {

    protected T mView;

    public void setView(T mView) {
        this.mView = mView;
    }

    public void onViewCreated(Bundle savedInstanceState) {
        //Used in child class if needed
    }
}
