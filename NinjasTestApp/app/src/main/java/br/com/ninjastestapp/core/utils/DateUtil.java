package br.com.ninjastestapp.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class DateUtil {

    private static final String DATE_PATTERN = "yyyy-MM-dd\'T\'HH:mm:ss.SSS";

    @Inject
    public DateUtil() {

    }

    public String convertDateToOfferFormat(String date) {
        Locale BRAZIL = new Locale("pt", "BR");
        Date tempDate;
        try {
            tempDate = new SimpleDateFormat(DATE_PATTERN).parse(date);
        } catch (ParseException e) {
            return date;
        }

        Calendar cal = Calendar.getInstance(BRAZIL);
        cal.setTime(tempDate);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return day + " de " + new SimpleDateFormat("MMM", BRAZIL).format(cal.getTime());


    }
}
