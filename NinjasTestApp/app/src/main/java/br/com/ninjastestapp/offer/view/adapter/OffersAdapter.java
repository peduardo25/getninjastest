package br.com.ninjastestapp.offer.view.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.utils.DateUtil;
import br.com.ninjastestapp.core.view.ItemClickListener;
import br.com.ninjastestapp.offer.model.Offer;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.presenter.OffersAdapterPresenter;
import br.com.ninjastestapp.offer.view.OffersAdapterView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OffersHolder>
        implements OffersAdapterView{

    private OffersResponse offersResponse = new OffersResponse();

    private OffersHolder holder;

    private ItemClickListener<Offer> itemClickListener;

    @Inject
    OffersAdapterPresenter adapterPresenter;

    @Inject
    DateUtil dateUtil;

    @Inject
    public OffersAdapter(){
    }

    @Override
    public OffersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OffersHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.offer_item, parent, false));
    }

    @Override
    public void onBindViewHolder(OffersHolder holder, int position) {
        adapterPresenter.setView(this);
        this.holder = holder;
        Offer offer = offersResponse.getOffers().get(position);
        holder.tvTitle.setText(offer.getEmbeddedResource().getRequest().getTitle());
        holder.tvName.setText(offer.getEmbeddedResource().getRequest().getEmbeddedResource().getUser().getName());
        holder.tvPlace.setText(
                offer.getEmbeddedResource()
                        .getRequest().getEmbeddedResource()
                        .getAddress().getNeighborhood() + " - "
                        + offer.getEmbeddedResource().getRequest()
                        .getEmbeddedResource().getAddress().getCity());
        holder.tvDate.setText(dateUtil.convertDateToOfferFormat(
                offer.getEmbeddedResource().getRequest().getCreatedAt())
        );
        adapterPresenter.checkOfferState(
                offer.getState()
        );

        holder.offerCard.setOnClickListener(v -> {
              itemClickListener.onItemClick(v.getId(), offer);
        });

    }

    @Override
    public int getItemCount() {
        return offersResponse.getOffers().size();
    }

    public void setOffers(OffersResponse offersResponse) {
        this.offersResponse = offersResponse;
    }

    public OffersResponse getOffersResponse() {
        return offersResponse;
    }

    public void setItemClickListener(ItemClickListener<Offer> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void showReadCircle() {
        holder.imvCircle.setImageResource(R.drawable.gray_circle);
    }

    @Override
    public void showUnreadCircle() {
        holder.imvCircle.setImageResource(R.drawable.blue_circle);
    }

    class OffersHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.offer_title)
        TextView tvTitle;
        @BindView(R.id.offer_name)
        TextView tvName;
        @BindView(R.id.offer_place)
        TextView tvPlace;
        @BindView(R.id.offer_date)
        TextView tvDate;
        @BindView(R.id.circle)
        ImageView imvCircle;
        @BindView(R.id.offer_card)
        CardView offerCard;

        OffersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
