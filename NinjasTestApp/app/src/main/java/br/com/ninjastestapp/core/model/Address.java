package br.com.ninjastestapp.core.model;

import com.google.gson.annotations.SerializedName;

import br.com.ninjastestapp.core.model.Geolocation;

public class Address {

    @SerializedName("city")
    private String city;

    @SerializedName("neighborhood")
    private String neighborhood;

    @SerializedName("uf")
    private String uf;

    @SerializedName("geolocation")
    private Geolocation geolocation;

    public String getCity() {
        return city;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getUf() {
        return uf;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }
}
