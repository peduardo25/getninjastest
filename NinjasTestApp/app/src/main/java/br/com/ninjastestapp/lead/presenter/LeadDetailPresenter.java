package br.com.ninjastestapp.lead.presenter;

import java.util.List;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.lead.view.LeadDetailView;

public interface LeadDetailPresenter extends AbstractPresenter<LeadDetailView> {
    void onDetailViewCreated(String leadUrl);

    void processInfoList(List<Info> infoList);

    DetailResponse getResponse();
}
