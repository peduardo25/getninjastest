package br.com.ninjastestapp.offer.module;

import br.com.ninjastestapp.offer.presenter.OfferDetailPresenter;
import br.com.ninjastestapp.offer.presenter.OffersAdapterPresenter;
import br.com.ninjastestapp.offer.presenter.OffersPresenter;
import br.com.ninjastestapp.offer.presenter.impl.OfferDetailPresenterImpl;
import br.com.ninjastestapp.offer.presenter.impl.OffersAdapterPresenterImpl;
import br.com.ninjastestapp.offer.presenter.impl.OffersPresenterImpl;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import br.com.ninjastestapp.offer.repository.impl.OffersRepositoryImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class OffersModule {

    @Provides
    public OffersPresenter getOffersPresenterProvider(OffersPresenterImpl impl) {
        return impl;
    }

    @Provides
    public OffersAdapterPresenter getOffersAdapterPresenterProvider(OffersAdapterPresenterImpl impl){
        return impl;
    }

    @Provides
    public OfferDetailPresenter getOfferDetailPresenterProvider(OfferDetailPresenterImpl impl){
        return impl;
    }
    @Provides
    public OffersRepository getOffersRepositoryProvider(OffersRepositoryImpl impl) {
        return impl;
    }
}
