package br.com.ninjastestapp.lead.networking;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface LeadAPIService {

    @GET
    Observable<LeadsResponse> getLeads(@Url String url);

    @GET
    Observable<DetailResponse> getLeadDetail(@Url String url);

}
