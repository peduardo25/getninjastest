package br.com.ninjastestapp.home.view;

import br.com.ninjastestapp.core.view.CoreView;

public interface HomeView extends CoreView{
    void setupViewPager();

    void showUrlRequestErrorMsg();
}
