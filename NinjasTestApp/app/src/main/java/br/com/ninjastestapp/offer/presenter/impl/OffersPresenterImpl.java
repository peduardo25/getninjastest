package br.com.ninjastestapp.offer.presenter.impl;

import javax.inject.Inject;

import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.presenter.OffersPresenter;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import br.com.ninjastestapp.offer.view.OffersView;

public class OffersPresenterImpl extends AbstractPresenterImpl<OffersView>
        implements OffersPresenter {

    private OffersRepository offersRepository;

    @Inject
    public OffersPresenterImpl(OffersRepository offersRepository) {
        this.offersRepository = offersRepository;
    }

    @Override
    public void onViewCreated() {
        offersRepository.getOffers().subscribe(offersResponse -> {
            mView.updateOffersListScreen(offersResponse);
            mView.hideProgressBar();
        }, throwable -> {
            mView.showRequestErrorMsg();
            mView.hideProgressBar();
        });

    }

    @Override
    public void updateList(OffersResponse offersResponse) {
        offersRepository.updateOffers(offersResponse.getLinks().getUrl().getHref())
                .subscribe(offersResponse1 -> {
                    mView.onUpdateFinished(offersResponse1);
                    mView.hideProgressBar();
                }, throwable -> {
                    mView.showRequestErrorMsg();
                    mView.hideProgressBar();
                });
    }
}
