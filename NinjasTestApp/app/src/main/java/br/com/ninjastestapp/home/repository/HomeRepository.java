package br.com.ninjastestapp.home.repository;

import android.content.Context;

import br.com.ninjastestapp.home.model.RootResponse;
import rx.Observable;

public interface HomeRepository {
    Observable<RootResponse> getRootUrls();

    void saveOfferUrl(String url);

    void saveLeadUrl(String url);
}
