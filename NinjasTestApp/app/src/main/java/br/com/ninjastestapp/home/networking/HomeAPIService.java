package br.com.ninjastestapp.home.networking;

import br.com.ninjastestapp.home.model.RootResponse;
import retrofit2.http.GET;
import rx.Observable;

public interface HomeAPIService {

    @GET("/")
    Observable<RootResponse> getBaseUrls();
}
