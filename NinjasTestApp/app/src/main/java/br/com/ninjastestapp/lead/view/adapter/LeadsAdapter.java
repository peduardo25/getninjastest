package br.com.ninjastestapp.lead.view.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.utils.DateUtil;
import br.com.ninjastestapp.core.view.ItemClickListener;
import br.com.ninjastestapp.lead.model.Lead;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.LeadsHolder>{

    private LeadsResponse leadsResponse = new LeadsResponse();

    private Context context;

    private ItemClickListener<Lead> itemClickListener;

    @Inject
    DateUtil dateUtil;

    @Inject
    public LeadsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public LeadsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LeadsAdapter.LeadsHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.lead_item, parent, false));
    }

    @Override
    public void onBindViewHolder(LeadsHolder holder, int position) {
        Lead lead = leadsResponse.getLeads().get(position);

        holder.tvTitle.setText(lead.getEmbedeed().getRequest().getTitle());

        holder.tvName.setText(lead.getEmbedeed().getUser().getName());

        String place = String.format(context.getString(R.string.place_label),
                lead.getEmbedeed().getAddress().getNeighborhood(),
                lead.getEmbedeed().getAddress().getUf());

        holder.tvPlace.setText(place);

        holder.tvDate.setText(dateUtil.convertDateToOfferFormat(
                lead.getCreatedAt())
        );

        holder.leadCard.setOnClickListener(v -> itemClickListener.onItemClick(v.getId(), lead));
    }

    @Override
    public int getItemCount() {
        return leadsResponse.getLeads().size();
    }

    public void setLeadsResponse(LeadsResponse leadsResponse) {
        this.leadsResponse = leadsResponse;
    }

    public LeadsResponse getLeadsResponse() {
        return leadsResponse;
    }

    public void setItemClickListener(ItemClickListener<Lead> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    class LeadsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lead_title)
        TextView tvTitle;
        @BindView(R.id.lead_name)
        TextView tvName;
        @BindView(R.id.lead_place)
        TextView tvPlace;
        @BindView(R.id.lead_date)
        TextView tvDate;
        @BindView(R.id.lead_card)
        CardView leadCard;

        LeadsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
