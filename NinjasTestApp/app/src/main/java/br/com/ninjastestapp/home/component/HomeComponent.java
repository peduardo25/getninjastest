package br.com.ninjastestapp.home.component;

import br.com.ninjastestapp.core.component.NetworkComponent;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.scope.ActivityScope;
import br.com.ninjastestapp.home.module.HomeModule;
import br.com.ninjastestapp.offer.module.OffersModule;
import br.com.ninjastestapp.home.view.impl.MainScreenActivity;
import dagger.Component;

@Component(modules = {AppModule.class, HomeModule.class}, dependencies = {NetworkComponent.class})
@ActivityScope
public interface HomeComponent {

    void inject(MainScreenActivity activity);
}
