package br.com.ninjastestapp.offer.view;

import br.com.ninjastestapp.core.view.CoreView;
import br.com.ninjastestapp.offer.model.OffersResponse;

public interface OffersView extends CoreView {
    void showRequestErrorMsg();

    void updateOffersListScreen(OffersResponse offersResponse);

    void onUpdateFinished(OffersResponse offersResponse1);

    void hideProgressBar();
}
