package br.com.ninjastestapp.core.view;

public interface ItemClickListener<T> {

    void onItemClick(int resId, T item);
}
