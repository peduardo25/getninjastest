package br.com.ninjastestapp.lead.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.core.utils.NinjasUtil;
import br.com.ninjastestapp.lead.presenter.LeadDetailPresenter;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import br.com.ninjastestapp.lead.view.LeadDetailView;

public class LeadDetailPresenterImpl extends AbstractPresenterImpl<LeadDetailView>
        implements LeadDetailPresenter {


    private LeadsRepository leadsRepository;

    private DetailResponse response;

    @Inject
    NinjasUtil ninjasUtil;

    @Inject
    public LeadDetailPresenterImpl(LeadsRepository leadsRepository) {
        this.leadsRepository = leadsRepository;
    }

    @Override
    public void onDetailViewCreated(final String leadUrl) {
        leadsRepository.getLeadDetail(leadUrl).subscribe(detailResponse -> {
            response = detailResponse;
            mView.fillDetailScreen(detailResponse);
            mView.hideProgressBar();
        }, throwable -> {
            mView.showDetailRequestError();
            mView.hideProgressBar();
        });
    }

    @Override
    public void processInfoList(List<Info> infoList) {
        for (Info info : infoList) {
            if (info.getValue().size() == 1) {
                mView.fillLeadInfo(info.getLabel(), info.getValue().get(0));
            } else {
                String result = ninjasUtil.addCommaToStr(info.getValue());

                mView.fillLeadInfo(info.getLabel(), result.length() > 0 ?
                        result.substring(0, result.length() - 2) : "");
            }
        }
        mView.hideProgressBar();
    }

    @Override
    public DetailResponse getResponse(){
        return response;
    }

}
