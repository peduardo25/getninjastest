package br.com.ninjastestapp.home.repository.impl;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.home.model.RootResponse;
import br.com.ninjastestapp.home.networking.HomeAPIService;
import br.com.ninjastestapp.home.repository.HomeRepository;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeRepositoryImpl implements HomeRepository {

    Retrofit retrofit;

    Context context;

    @Inject
    public HomeRepositoryImpl(Retrofit retrofit,  Context context) {
        this.retrofit = retrofit;
        this.context = context;
    }

    @Override
    public Observable<RootResponse> getRootUrls() {
        return retrofit.create(HomeAPIService.class).getBaseUrls()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void saveOfferUrl(final String url) {
        SharedPreferences pref = context.getSharedPreferences(
                Constants.URL_PREF_NAME, Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.OFFER_URL_KEY, url);
        editor.apply();

    }

    @Override
    public void saveLeadUrl(final String url){
        SharedPreferences pref = context.getSharedPreferences(
                Constants.URL_PREF_NAME, Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.LEAD_URL_KEY, url);
        editor.apply();
    }
}
