package br.com.ninjastestapp.home.presenter.impl;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.home.presenter.HomePresenter;
import br.com.ninjastestapp.home.repository.HomeRepository;
import br.com.ninjastestapp.home.view.HomeView;


public class HomePresenterImpl extends AbstractPresenterImpl<HomeView> implements HomePresenter {


    HomeRepository homeRepository;

    @Inject
    public HomePresenterImpl(HomeRepository homeRepository) {
        this.homeRepository = homeRepository;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        super.onViewCreated(savedInstanceState);

        homeRepository.getRootUrls().subscribe(rootResponse -> {
            homeRepository.saveLeadUrl(rootResponse.getLeadsHref());
            homeRepository.saveOfferUrl(rootResponse.getOffersHref());
            mView.setupViewPager();
        }, throwable -> {
            mView.showUrlRequestErrorMsg();
        });

    }

}
