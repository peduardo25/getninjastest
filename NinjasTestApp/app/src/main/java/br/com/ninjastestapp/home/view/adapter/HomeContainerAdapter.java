package br.com.ninjastestapp.home.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeContainerAdapter extends FragmentPagerAdapter {

    private List<Page> pages = new ArrayList<>();

    public HomeContainerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return this.pages.get(position).fragment;
    }

    @Override
    public int getCount() {
        return this.pages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.pages.get(position).title;
    }

    public void addPage(String title, Fragment fragment) {
        this.pages.add(new Page(title, fragment));
    }

    private static class Page {
        private String title;
        private Fragment fragment;
        public Page(String title, Fragment fragment) {
            this.title = title;
            this.fragment = fragment;
        }
    }
}