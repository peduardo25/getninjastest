package br.com.ninjastestapp.core.model;

import com.google.gson.annotations.SerializedName;

public class Link {

    @SerializedName("href")
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}

