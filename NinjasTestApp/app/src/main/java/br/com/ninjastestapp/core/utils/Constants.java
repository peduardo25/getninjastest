package br.com.ninjastestapp.core.utils;

public interface Constants {

    String OFFER_FRAGMENT = "br.com.ninjastestapp.offer.view.impl.OfferFragment";
    String LEAD_FRAGMENT = "br.com.ninjastestapp.lead.view.impl.LeadFragment";
    String OFFER_DETAIL_ACTIVITY = "br.com.ninjastestapp.offer.view.impl.OfferDetailActivity";
    String LEAD_DETAIL_ACTIVITY = "br.com.ninjastestapp.lead.view.impl.LeadDetailActivity";
    String URL_PREF_NAME = "urls_base_rep";
    String OFFER_URL_KEY = "offer_url";
    String LEAD_URL_KEY = "lead_key";
    String DETAIL_URL_KEY = "detail_url_key";
    String WHATSAPP_PACKAGE = "com.whatsapp";
    String LEAD_TITLE_KEY = "lead_title_key";
}
