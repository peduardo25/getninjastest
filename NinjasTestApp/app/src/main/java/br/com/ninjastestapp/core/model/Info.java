package br.com.ninjastestapp.core.model;

import java.util.List;

public class Info {
    private String label;
    private List<String> value;

    public Info(String label, List<String> value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public List<String>  getValue() {
        return value;
    }

}
