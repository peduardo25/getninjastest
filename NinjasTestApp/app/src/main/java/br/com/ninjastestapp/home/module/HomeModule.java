package br.com.ninjastestapp.home.module;

import br.com.ninjastestapp.home.presenter.HomePresenter;
import br.com.ninjastestapp.home.presenter.impl.HomePresenterImpl;
import br.com.ninjastestapp.home.repository.HomeRepository;
import br.com.ninjastestapp.home.repository.impl.HomeRepositoryImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    @Provides
    public HomePresenter getHomePresenterProvider(HomePresenterImpl impl) {
        return impl;
    }

    @Provides
    public HomeRepository getHomeRepositoryProvider(HomeRepositoryImpl impl) {
        return impl;
    }
}
