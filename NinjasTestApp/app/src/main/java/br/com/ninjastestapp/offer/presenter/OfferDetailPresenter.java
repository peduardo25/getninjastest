package br.com.ninjastestapp.offer.presenter;

import java.util.List;

import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.model.Phone;
import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.offer.view.OfferDetailview;

public interface OfferDetailPresenter extends AbstractPresenter<OfferDetailview> {
    void onDetailViewCreated(String string);

    void processInfoList(List<Info> infoList);
}
