package br.com.ninjastestapp.offer.presenter;

import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.offer.view.OffersAdapterView;

public interface OffersAdapterPresenter extends AbstractPresenter<OffersAdapterView> {
    void checkOfferState(String state);
}
