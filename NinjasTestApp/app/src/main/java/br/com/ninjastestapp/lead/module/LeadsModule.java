package br.com.ninjastestapp.lead.module;

import br.com.ninjastestapp.lead.presenter.LeadDetailPresenter;
import br.com.ninjastestapp.lead.presenter.LeadPresenter;
import br.com.ninjastestapp.lead.presenter.impl.LeadDetailPresenterImpl;
import br.com.ninjastestapp.lead.presenter.impl.LeadPresenterImpl;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import br.com.ninjastestapp.lead.repository.impl.LeadsRepositoryImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class LeadsModule {

    @Provides
    public LeadPresenter getLeadPresenterImplProvider(LeadPresenterImpl impl) {
        return impl;
    }

    @Provides
    public LeadDetailPresenter getLeadPresenterDetailImplProvider(LeadDetailPresenterImpl impl){
        return impl;
    }

    @Provides
    public LeadsRepository getLeadsRepositoryImplProvider(LeadsRepositoryImpl impl) {
        return impl;
    }
}
