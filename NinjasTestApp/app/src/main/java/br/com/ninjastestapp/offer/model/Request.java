package br.com.ninjastestapp.offer.model;

import com.google.gson.annotations.SerializedName;

import br.com.ninjastestapp.core.model.Address;
import br.com.ninjastestapp.core.model.User;

public class Request {

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("title")
    private String title;

    @SerializedName("_embedded")
    private Embedded embeddedResource;

    public String getCreatedAt() {
        return createdAt;
    }

    public Embedded getEmbeddedResource() {
        return embeddedResource;
    }

    public String getTitle() {
        return title;
    }

    public class Embedded {

        @SerializedName("user")
        private User user;

        @SerializedName("address")
        private Address address;

        public User getUser() {
            return user;
        }

        public Address getAddress() {
            return address;
        }
    }
}
