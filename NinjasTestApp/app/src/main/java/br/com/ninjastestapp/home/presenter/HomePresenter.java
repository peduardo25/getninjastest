package br.com.ninjastestapp.home.presenter;

import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.home.view.HomeView;

public interface HomePresenter extends AbstractPresenter<HomeView> {
}
