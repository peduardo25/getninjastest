package br.com.ninjastestapp.lead.view;

import br.com.ninjastestapp.core.view.CoreView;
import br.com.ninjastestapp.lead.model.LeadsResponse;

public interface LeadView extends CoreView{
    void onUpdateFinished(LeadsResponse leadsResponse);

    void onLeadsLoaded(LeadsResponse leadsResponse);

    void showLeadRequestFail();

    void hideProgressBar();
}
