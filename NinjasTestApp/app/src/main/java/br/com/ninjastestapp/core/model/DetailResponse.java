package br.com.ninjastestapp.core.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.Address;
import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.core.model.User;

public class DetailResponse {

    @SerializedName("distance")
    private long distance;

    @SerializedName("lead_price")
    private int leadPrice;

    @SerializedName("title")
    private String title;

    private ActionLinks rootLinks;

    @SerializedName("_embedded")
    private Embedded embeddedResource;

    public long getDistance() {
        return distance;
    }

    public int getLeadPrice() {
        return leadPrice;
    }

    public String getTitle() {
        return title;
    }

    public ActionLinks getRootLinks() {
        return rootLinks;
    }

    public Embedded getEmbeddedResource() {
        return embeddedResource;
    }

    public class Embedded{

        private static final String LABEL_KEY = "label";
        private static final String VALUE_KEY = "value";

        @SerializedName("user")
        private User user;

        @SerializedName("address")
        private Address address;

        @SerializedName("info")
        private JsonArray array;

        public User getUser() {
            return user;
        }

        public Address getAddress() {
            return address;
        }

        public List<Info> getInfoList(){

            ArrayList<Info> result = new ArrayList<>();

            for (JsonElement json : array) {
                JsonObject jsonObject = json.getAsJsonObject();
                String infoLabel = jsonObject.get(LABEL_KEY).getAsString();
                List<String> infoValues = new ArrayList<>();

                JsonElement valueElement = jsonObject.get(VALUE_KEY);
                if (valueElement.isJsonPrimitive()) {
                    infoValues.add(valueElement.getAsString());
                } else {
                    for (JsonElement jsonValue : valueElement.getAsJsonArray()){
                        infoValues.add(jsonValue.getAsString());
                    }
                }

                result.add(new Info(infoLabel, infoValues));
            }
            return result;
        }
    }

    public class ActionLinks {

        @SerializedName("accept")
        private Link accpet;

        @SerializedName("reject")
        private Link reject;

        public Link getAccpet() {
            return accpet;
        }

        public Link getReject() {
            return reject;
        }
    }
}
