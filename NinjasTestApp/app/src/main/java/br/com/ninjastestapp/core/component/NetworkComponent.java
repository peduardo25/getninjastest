package br.com.ninjastestapp.core.component;

import javax.inject.Singleton;

import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.module.NetworkModule;
import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {

    Retrofit retrofit();

}
