package br.com.ninjastestapp.core.utils;

import java.text.DecimalFormat;
import java.util.List;

import javax.inject.Inject;

import br.com.ninjastestapp.core.model.Phone;

public class NinjasUtil {

    @Inject
    public NinjasUtil(){

    }

    public String concatPhones(final List<Phone> phones) {
        String concatPhones = "";
        for (Phone phone : phones) {
            concatPhones += phone.getNumber() + "|";
        }
        return concatPhones.substring(0, concatPhones.length() - 1);
    }

    public String calcDistanceInKm(final long distance) {
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        return decimalFormat.format((double) distance / 10000);
    }

    public String addCommaToStr(List<String> values) {
        StringBuilder result = new StringBuilder();
        for(String value : values) {
            result.append(value);
            result.append(", ");
        }

        return result.toString();
    }
}
