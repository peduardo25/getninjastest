package br.com.ninjastestapp.home.model;

import com.google.gson.annotations.SerializedName;

import br.com.ninjastestapp.core.model.Link;

public class RootResponse {

    @SerializedName("_links")
    private RootLinks links;

    public String getOffersHref() {
        return links.offers.getHref();
    }

    public String getLeadsHref() {
        return links.leads.getHref();
    }


    public class RootLinks {

        @SerializedName("offers")
        private Link offers;

        @SerializedName("leads")
        private Link leads;

    }
}
