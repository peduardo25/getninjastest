package br.com.ninjastestapp.offer.model;

import com.google.gson.annotations.SerializedName;

import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.core.model.RootLinks;

public class Offer extends RootLinks{

    @SerializedName("state")
    private String state;

    @SerializedName("_links")
    private RootLinks links;

    @SerializedName("_embedded")
    private Embedded embeddedResource;

    public RootLinks getLinks() {
        return links;
    }

    public String getState() {
        return state;
    }

    public Embedded getEmbeddedResource() {
        return embeddedResource;
    }


    public class Embedded {

        @SerializedName("request")
        private Request request;

        public Request getRequest() {
            return request;
        }
    }
}
