package br.com.ninjastestapp.lead.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.RootLinks;

public class LeadsResponse extends RootLinks {

    @SerializedName("leads")
    private List<Lead> leads = new ArrayList<>();

    @SerializedName("_links")
    private RootLinks links;

    public List<Lead> getLeads() {
        return leads;
    }

    public RootLinks getLinks() {
        return links;
    }
}
