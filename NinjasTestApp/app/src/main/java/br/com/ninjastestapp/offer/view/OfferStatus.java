package br.com.ninjastestapp.offer.view;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface OfferStatus {

    @StringDef({State.READ,
            State.UNREAD,
    })

    @Retention(RetentionPolicy.SOURCE)
    @interface State {
        String READ = "read";
        String UNREAD = "unread";
    }
}
