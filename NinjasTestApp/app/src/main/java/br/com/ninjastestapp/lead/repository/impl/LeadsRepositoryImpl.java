package br.com.ninjastestapp.lead.repository.impl;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import br.com.ninjastestapp.lead.networking.LeadAPIService;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class LeadsRepositoryImpl implements LeadsRepository {

    Retrofit retrofit;

    Context context;

    @Inject
    public LeadsRepositoryImpl(Retrofit retrofit, Context context) {
        this.retrofit = retrofit;
        this.context = context;
    }

    @Override
    public Observable<LeadsResponse> getLeads() {
        return updateLeads(retrieveLeadUrl());
    }

    @Override
    public Observable<DetailResponse> getLeadDetail(final String url) {
        return retrofit.create(LeadAPIService.class).getLeadDetail(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<LeadsResponse> updateLeads(String url) {
        return retrofit.create(LeadAPIService.class).getLeads(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private String retrieveLeadUrl() {
        SharedPreferences pref = context.getSharedPreferences(
                Constants.URL_PREF_NAME, Context.MODE_PRIVATE
        );

        return pref.getString(Constants.LEAD_URL_KEY, null);
    }
}
