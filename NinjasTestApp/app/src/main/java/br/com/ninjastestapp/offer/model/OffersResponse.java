package br.com.ninjastestapp.offer.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.core.model.RootLinks;

public class OffersResponse extends RootLinks{

    @SerializedName("offers")
    private List<Offer> offers = new ArrayList<>();

    @SerializedName("_links")
    private RootLinks links;

    public List<Offer> getOffers() {
        return offers;
    }

    public RootLinks getLinks() {
        return links;
    }


}
