package br.com.ninjastestapp.lead.model;

import com.google.gson.annotations.SerializedName;

public class Request {

    @SerializedName("title")
    private String title;

    public String getTitle() {
        return title;
    }
}
