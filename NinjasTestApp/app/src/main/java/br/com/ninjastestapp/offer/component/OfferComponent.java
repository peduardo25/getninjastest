package br.com.ninjastestapp.offer.component;

import br.com.ninjastestapp.core.component.NetworkComponent;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.scope.ActivityScope;
import br.com.ninjastestapp.offer.module.OffersModule;
import br.com.ninjastestapp.offer.view.impl.OfferDetailActivity;
import br.com.ninjastestapp.offer.view.impl.OfferFragment;
import dagger.Component;

@Component(modules = {AppModule.class, OffersModule.class}, dependencies = {NetworkComponent.class})
@ActivityScope
public interface OfferComponent {

    void inject(OfferFragment fragment);

    void inject(OfferDetailActivity activity);
}
