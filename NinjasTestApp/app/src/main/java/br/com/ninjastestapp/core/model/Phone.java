package br.com.ninjastestapp.core.model;

import com.google.gson.annotations.SerializedName;

public class Phone {

    @SerializedName("number")
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
