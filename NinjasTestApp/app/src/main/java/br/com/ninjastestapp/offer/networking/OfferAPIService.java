package br.com.ninjastestapp.offer.networking;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.offer.model.OffersResponse;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface OfferAPIService {

    @GET
    Observable<OffersResponse> getOffers(@Url String url);

    @GET
    Observable<DetailResponse> getOfferDetail(@Url String url);
}
