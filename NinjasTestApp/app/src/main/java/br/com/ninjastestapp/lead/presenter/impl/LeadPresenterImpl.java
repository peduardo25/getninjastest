package br.com.ninjastestapp.lead.presenter.impl;

import javax.inject.Inject;

import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import br.com.ninjastestapp.lead.presenter.LeadPresenter;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import br.com.ninjastestapp.lead.view.LeadView;


public class LeadPresenterImpl extends AbstractPresenterImpl<LeadView>
        implements LeadPresenter {

    LeadsRepository leadsRepository;

    @Inject
    public LeadPresenterImpl(LeadsRepository leadsRepository) {
        this.leadsRepository = leadsRepository;
    }

    @Override
    public void onViewCreated() {
        leadsRepository.getLeads().subscribe(leadsResponse -> {
           mView.onLeadsLoaded(leadsResponse);
            mView.hideProgressBar();
        }, throwable -> {
            mView.showLeadRequestFail();
            mView.hideProgressBar();
        });

    }

    @Override
    public void updateList(LeadsResponse leadsResponse) {
        leadsRepository.updateLeads(leadsResponse.getLinks().getUrl().getHref())
                .subscribe(leadsResponse1 -> {
                    mView.onUpdateFinished(leadsResponse);
                    mView.hideProgressBar();
                }, throwable -> {
                    mView.showLeadRequestFail();
                    mView.hideProgressBar();
                });
    }
}
