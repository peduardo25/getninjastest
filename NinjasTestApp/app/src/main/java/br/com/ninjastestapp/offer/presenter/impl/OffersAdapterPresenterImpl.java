package br.com.ninjastestapp.offer.presenter.impl;

import javax.inject.Inject;

import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.offer.presenter.OffersAdapterPresenter;
import br.com.ninjastestapp.offer.view.OfferStatus;
import br.com.ninjastestapp.offer.view.OffersAdapterView;

public class OffersAdapterPresenterImpl extends AbstractPresenterImpl<OffersAdapterView>
        implements OffersAdapterPresenter {


    @Inject
    public OffersAdapterPresenterImpl(){

    }

    @Override
    public void checkOfferState(String state) {
        if(state.equals(OfferStatus.State.READ)) {
            mView.showReadCircle();
        } else {
            mView.showUnreadCircle();
        }
    }
}
