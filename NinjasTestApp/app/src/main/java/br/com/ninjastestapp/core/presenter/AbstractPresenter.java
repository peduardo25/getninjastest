package br.com.ninjastestapp.core.presenter;

import android.os.Bundle;

import br.com.ninjastestapp.core.view.CoreView;

public interface AbstractPresenter<T extends CoreView>  {

    void setView(T mView);

    void onViewCreated(Bundle savedInstanceState);

}

