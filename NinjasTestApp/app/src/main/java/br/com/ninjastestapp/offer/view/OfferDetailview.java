package br.com.ninjastestapp.offer.view;

import android.annotation.SuppressLint;

import br.com.ninjastestapp.core.view.CoreView;
import br.com.ninjastestapp.core.model.DetailResponse;

public interface OfferDetailview extends CoreView {
    void onDetailRequestError();

    void fillDetailScreen(DetailResponse offerDetailResponse);

    @SuppressLint("InflateParams")
    void fillInfo(String label, String value);

    void hideProgressBar();
}
