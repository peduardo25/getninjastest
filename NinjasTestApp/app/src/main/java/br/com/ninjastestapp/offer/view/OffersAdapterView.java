package br.com.ninjastestapp.offer.view;

import br.com.ninjastestapp.core.view.CoreView;

public interface OffersAdapterView extends CoreView {
    void showReadCircle();

    void showUnreadCircle();
}
