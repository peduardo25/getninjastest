package br.com.ninjastestapp.offer.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.application.NinjasApplication;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.core.view.ItemClickListener;
import br.com.ninjastestapp.offer.component.DaggerOfferComponent;
import br.com.ninjastestapp.offer.model.Offer;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.presenter.OffersPresenter;
import br.com.ninjastestapp.offer.view.OffersView;
import br.com.ninjastestapp.offer.view.adapter.OffersAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferFragment extends Fragment implements OffersView, ItemClickListener<Offer>{

    @BindView(R.id.rv_offer)
    RecyclerView rvOffers;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    OffersPresenter offersPresenter;

    @Inject
    OffersAdapter offersAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,

                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.offer_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupInjection();
        setupRecyclerView();
        setupSwipeRefresh();
        offersPresenter.setView(this);
        progressBar.setVisibility(View.VISIBLE);
        offersPresenter.onViewCreated();


    }

    private void setupInjection(){
        DaggerOfferComponent.builder()
                .networkComponent(((NinjasApplication) getActivity()
                        .getApplication()).getNetworkInjection())
                .appModule(new AppModule((NinjasApplication) getActivity().getApplication()))
                .build()
                .inject(this);
    }


    private void setupRecyclerView() {
        rvOffers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvOffers.setAdapter(offersAdapter);
        offersAdapter.setItemClickListener(this);
    }

    private void setupSwipeRefresh(){
        refreshLayout.setColorSchemeResources(R.color.screen_background);
        refreshLayout.setOnRefreshListener(() -> offersPresenter.updateList(
                offersAdapter.getOffersResponse())
        );
    }

    @Override
    public void showRequestErrorMsg() {
        Toast.makeText(getActivity().getApplicationContext(),
                getString(R.string.connection_error_msg),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateOffersListScreen(OffersResponse offersResponse) {
        updateAdapter(offersResponse);
    }

    @Override
    public void onUpdateFinished(OffersResponse offersResponse1) {
        updateAdapter(offersResponse1);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void updateAdapter(OffersResponse offersResponse) {
        offersAdapter.setOffers(offersResponse);
        offersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int resId, Offer item) {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.DETAIL_URL_KEY, item.getLinks().getUrl().getHref());

        Intent it = new Intent(Constants.OFFER_DETAIL_ACTIVITY);
        it.putExtras(bundle);
        startActivity(it);

    }
}
