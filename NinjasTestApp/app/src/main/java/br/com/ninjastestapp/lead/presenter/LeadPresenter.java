package br.com.ninjastestapp.lead.presenter;

import br.com.ninjastestapp.core.presenter.AbstractPresenter;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import br.com.ninjastestapp.lead.view.LeadView;

public interface LeadPresenter extends AbstractPresenter<LeadView>{
    void onViewCreated();

    void updateList(LeadsResponse leadsResponse);
}
