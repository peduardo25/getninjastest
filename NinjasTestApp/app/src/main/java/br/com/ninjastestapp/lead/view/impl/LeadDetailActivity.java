package br.com.ninjastestapp.lead.view.impl;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.application.NinjasApplication;
import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.core.view.impl.BaseActivity;
import br.com.ninjastestapp.lead.component.DaggerLeadsComponent;
import br.com.ninjastestapp.lead.presenter.LeadDetailPresenter;
import br.com.ninjastestapp.lead.view.LeadDetailView;
import butterknife.ButterKnife;

public class LeadDetailActivity extends BaseActivity implements LeadDetailView {

    @Inject
    LeadDetailPresenter leadDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_detail);
        setupButterKnife();
        setupInjection();
        setupActionBar(getIntent().getExtras().getString(Constants.LEAD_TITLE_KEY));
        leadDetailPresenter.setView(this);
        getProgressBar().setVisibility(View.VISIBLE);
        leadDetailPresenter.onDetailViewCreated(
                getIntent().getExtras().getString(Constants.DETAIL_URL_KEY)
        );
    }

    private void setupButterKnife() {
        ButterKnife.bind(this);
    }

    private void setupInjection() {
        DaggerLeadsComponent.builder()
                .networkComponent(((NinjasApplication) getApplication()).getNetworkInjection())
                .appModule(new AppModule((NinjasApplication) getApplication()))
                .build()
                .inject(this);
    }

    @Override
    public void fillDetailScreen(DetailResponse detailResponse) {
        setupMap(detailResponse.getEmbeddedResource().getAddress().getGeolocation());
        fillHeaderData(detailResponse);

        fillCostumerContact(detailResponse.getEmbeddedResource().getUser(),
                ContextCompat.getColor(getApplicationContext(),
                        R.color.lead_costumer_detail_background),
                getString(R.string.lead_contact_msg));

        leadDetailPresenter.processInfoList(detailResponse.getEmbeddedResource().getInfoList());

    }

    @Override
    public void fillLeadInfo(final String label, final String value) {
        super.fillInfo(label, value);

    }

    @Override
    public void showDetailRequestError() {
        Toast.makeText(
                getApplicationContext(),
                getString(R.string.detail_lead_request_error_msg),
                Toast.LENGTH_LONG
        ).show();
    }

    @Override
    public void hideProgressBar() {
        getProgressBar().setVisibility(View.GONE);
    }

    public void call(View view) {
        Intent intent = new Intent(
                Intent.ACTION_CALL, Uri.parse("tel:" +
                leadDetailPresenter.getResponse()
                        .getEmbeddedResource()
                        .getUser()
                        .getEmbeddedResource()
                        .getPhones()
                        .get(0)
                        .getNumber())
        );

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
           Toast.makeText(
                   getApplicationContext(), R.string.permission_denied_msg, Toast.LENGTH_LONG
           ).show();
                   return;
        }
        startActivity(intent);
    }

    public void whatsapp(View view){
        PackageManager pm = getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = getString(R.string.whatsapp_default_msg);

            PackageInfo info=pm.getPackageInfo(Constants.WHATSAPP_PACKAGE,
                    PackageManager.GET_META_DATA);

            waIntent.setPackage(Constants.WHATSAPP_PACKAGE);

            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, getString(R.string.share_msg)));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, R.string.whatsapp_not_installed_msg, Toast.LENGTH_SHORT)
                    .show();
        }

    }
}
