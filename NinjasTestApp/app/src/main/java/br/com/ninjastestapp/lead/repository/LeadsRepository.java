package br.com.ninjastestapp.lead.repository;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import rx.Observable;

public interface LeadsRepository {
    Observable<LeadsResponse> getLeads();

    Observable<DetailResponse> getLeadDetail(String url);

    Observable<LeadsResponse> updateLeads(String url);
}
