package br.com.ninjastestapp.offer.presenter.impl;

import java.util.List;

import javax.inject.Inject;

import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.presenter.impl.AbstractPresenterImpl;
import br.com.ninjastestapp.core.utils.NinjasUtil;
import br.com.ninjastestapp.offer.presenter.OfferDetailPresenter;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import br.com.ninjastestapp.offer.view.OfferDetailview;

public class OfferDetailPresenterImpl extends AbstractPresenterImpl<OfferDetailview>
        implements OfferDetailPresenter {

    private OffersRepository offersRepository;

    @Inject
    NinjasUtil ninjasUtil;

    @Inject
    public OfferDetailPresenterImpl(OffersRepository offersRepository) {
        this.offersRepository = offersRepository;
    }

    @Override
    public void onDetailViewCreated(final String url) {
        offersRepository.getOfferDetail(url).subscribe(offerDetailResponse -> {
            mView.fillDetailScreen(offerDetailResponse);
            mView.hideProgressBar();
        }, throwable -> {
            mView.onDetailRequestError();
            mView.hideProgressBar();
        });

    }

    @Override
    public void processInfoList(final List<Info> infoList) {
        for (Info info : infoList) {
            if (info.getValue().size() == 1) {
                mView.fillInfo(info.getLabel(), info.getValue().get(0));
            } else {
                String result = ninjasUtil.addCommaToStr(info.getValue());

                mView.fillInfo(info.getLabel(), result.length() > 0 ?
                        result.substring(0, result.length() - 2) : "");
            }
        }
        mView.hideProgressBar();
    }
}
