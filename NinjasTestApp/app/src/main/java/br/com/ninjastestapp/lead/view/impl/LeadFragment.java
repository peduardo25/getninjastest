package br.com.ninjastestapp.lead.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.application.NinjasApplication;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.core.view.ItemClickListener;
import br.com.ninjastestapp.lead.component.DaggerLeadsComponent;
import br.com.ninjastestapp.lead.model.Lead;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import br.com.ninjastestapp.lead.presenter.LeadPresenter;
import br.com.ninjastestapp.lead.view.LeadView;
import br.com.ninjastestapp.lead.view.adapter.LeadsAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadFragment extends Fragment implements LeadView, ItemClickListener<Lead>{


    @BindView(R.id.rv_lead)
    RecyclerView rvLeads;

    @BindView(R.id.lead_swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    LeadPresenter leadPresenter;

    @Inject
    LeadsAdapter leadsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,

                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lead_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupInjection();
        setupRecyclerView();
        setupSwipeRefresh();
        leadPresenter.setView(this);
        progressBar.setVisibility(View.VISIBLE);
        leadPresenter.onViewCreated();
    }

    private void setupInjection() {
        DaggerLeadsComponent.builder()
                .networkComponent(((NinjasApplication) getActivity()
                        .getApplication())
                        .getNetworkInjection())
                .appModule(new AppModule((NinjasApplication) getActivity().getApplication()))
                .build()
                .inject(this);
    }



    private void setupRecyclerView() {
        rvLeads.setLayoutManager(new LinearLayoutManager(getContext()));
        rvLeads.setAdapter(leadsAdapter);
        leadsAdapter.setItemClickListener(this);
    }

    private void setupSwipeRefresh(){
        refreshLayout.setColorSchemeResources(R.color.screen_background);
        refreshLayout.setOnRefreshListener(() -> leadPresenter.updateList(
                leadsAdapter.getLeadsResponse())
        );
    }

    @Override
    public void onUpdateFinished(LeadsResponse leadsResponse) {
        updateAdapter(leadsResponse);
        refreshLayout.setRefreshing(false);
    }

    private void updateAdapter(LeadsResponse leadsResponse) {
        leadsAdapter.setLeadsResponse(leadsResponse);
        leadsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLeadsLoaded(LeadsResponse leadsResponse) {
        updateAdapter(leadsResponse);
    }

    @Override
    public void showLeadRequestFail() {
        Toast.makeText(getActivity().getApplicationContext(),
                getString(R.string.connection_error_msg),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(int resId, Lead item) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.DETAIL_URL_KEY, item.getLinks().getUrl().getHref());
        bundle.putString(Constants.LEAD_TITLE_KEY, item.getEmbedeed().getRequest().getTitle());
        Intent it = new Intent(Constants.LEAD_DETAIL_ACTIVITY);
        it.putExtras(bundle);
        startActivity(it);
    }
}