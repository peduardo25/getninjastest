package br.com.ninjastestapp.offer.view.impl;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import br.com.ninjastestapp.R;
import br.com.ninjastestapp.core.application.NinjasApplication;
import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.utils.Constants;
import br.com.ninjastestapp.core.view.impl.BaseActivity;
import br.com.ninjastestapp.offer.component.DaggerOfferComponent;
import br.com.ninjastestapp.offer.presenter.OfferDetailPresenter;
import br.com.ninjastestapp.offer.view.OfferDetailview;
import butterknife.ButterKnife;

public class OfferDetailActivity extends BaseActivity implements OfferDetailview {


    @Inject
    OfferDetailPresenter offerDetailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_detail);
        setupInjection();
        setupButterKnife();
        setupActionBar(getString(R.string.offer_detail_title));

        offerDetailPresenter.setView(this);
        getProgressBar().setVisibility(View.VISIBLE);
        offerDetailPresenter.onDetailViewCreated(getIntent().getExtras().getString(
                Constants.DETAIL_URL_KEY)
        );

    }

    private void setupInjection() {
        DaggerOfferComponent.builder()
                .networkComponent(((NinjasApplication) getApplication()).getNetworkInjection())
                .appModule(new AppModule((NinjasApplication) getApplication()))
                .build()
                .inject(this);
    }

    private void setupButterKnife() {
        ButterKnife.bind(this);
    }


    @Override
    public void onDetailRequestError() {
        Toast.makeText(
                getApplicationContext(),
                getString(R.string.detail_offer_request_error_msg),
                Toast.LENGTH_LONG
        ).show();
    }

    @Override
    public void fillDetailScreen(DetailResponse offerDetailResponse) {
        setupMap(offerDetailResponse.getEmbeddedResource().getAddress().getGeolocation());
        fillHeaderData(offerDetailResponse);

        fillCostumerContact(offerDetailResponse.getEmbeddedResource().getUser(), ContextCompat.getColor(getApplicationContext(),
                R.color.offer_costumer_detail_background), getString(R.string.contact_msg));

        offerDetailPresenter.processInfoList(
                offerDetailResponse.getEmbeddedResource().getInfoList()
        );
    }


    @Override
    public void fillInfo(final String label, final String value) {
        super.fillInfo(label, value);

    }

    @Override
    public void hideProgressBar() {
        getProgressBar().setVisibility(View.GONE);
    }

    public void accept(View view) {
        Toast.makeText(getApplicationContext(), R.string.accept_offer_msg, Toast.LENGTH_LONG).show();
    }

    public void reject(View view) {
        Toast.makeText(getApplicationContext(), R.string.reject_offer_msg, Toast.LENGTH_LONG).show();
    }
}
