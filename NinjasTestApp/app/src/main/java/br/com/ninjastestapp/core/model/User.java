package br.com.ninjastestapp.core.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.ninjastestapp.core.model.Phone;

public class User {

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("_embedded")
    private Embedded embeddedResource;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Embedded getEmbeddedResource() {
        return embeddedResource;
    }

    public class Embedded {

        @SerializedName("phones")
        private List<Phone> phones;

        public List<Phone> getPhones() {
            return phones;
        }
    }
}
