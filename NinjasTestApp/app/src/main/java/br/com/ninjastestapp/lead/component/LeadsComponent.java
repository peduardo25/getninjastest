package br.com.ninjastestapp.lead.component;

import br.com.ninjastestapp.core.component.NetworkComponent;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.scope.ActivityScope;
import br.com.ninjastestapp.lead.module.LeadsModule;
import br.com.ninjastestapp.lead.view.impl.LeadDetailActivity;
import br.com.ninjastestapp.lead.view.impl.LeadFragment;
import dagger.Component;

@Component (modules = {AppModule.class, LeadsModule.class}, dependencies = {NetworkComponent.class})
@ActivityScope
public interface LeadsComponent {

    void inject(LeadFragment fragment);

    void inject(LeadDetailActivity activity);
}
