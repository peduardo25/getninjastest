package br.com.ninjastestapp.core.application;

import android.app.Application;

import br.com.ninjastestapp.core.component.DaggerNetworkComponent;
import br.com.ninjastestapp.core.component.NetworkComponent;
import br.com.ninjastestapp.core.module.AppModule;
import br.com.ninjastestapp.core.module.NetworkModule;

public class NinjasApplication extends Application{

    private NetworkComponent networkInjection;

    @Override
    public void onCreate() {
        super.onCreate();

        this.networkInjection = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public NetworkComponent getNetworkInjection() {
        return this.networkInjection;
    }
}
