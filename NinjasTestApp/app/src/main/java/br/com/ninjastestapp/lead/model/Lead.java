package br.com.ninjastestapp.lead.model;

import com.google.gson.annotations.SerializedName;

import br.com.ninjastestapp.core.model.Address;
import br.com.ninjastestapp.core.model.RootLinks;
import br.com.ninjastestapp.core.model.User;

public class Lead extends RootLinks{

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("_links")
    private RootLinks links;

    @SerializedName("_embedded")
    private Embedded embedeed;

    public String getCreatedAt() {
        return createdAt;
    }

    public RootLinks getLinks() {
        return links;
    }

    public Embedded getEmbedeed() {
        return embedeed;
    }

    public class Embedded {
        @SerializedName("address")
        private Address address;

        @SerializedName("user")
        private User user;

        @SerializedName("request")
        private Request request;

        public Address getAddress() {
            return address;
        }

        public User getUser() {
            return user;
        }

        public Request getRequest() {
            return request;
        }
    }

}
