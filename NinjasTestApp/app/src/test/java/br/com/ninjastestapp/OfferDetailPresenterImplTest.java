package br.com.ninjastestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.utils.NinjasUtil;
import br.com.ninjastestapp.offer.presenter.impl.OfferDetailPresenterImpl;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import br.com.ninjastestapp.offer.view.OfferDetailview;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class OfferDetailPresenterImplTest {

    @Mock
    OffersRepository offersRepository;

    @Mock
    OfferDetailview offerDetailview;

    @Mock
    NinjasUtil ninjasUtil;

    @InjectMocks
    OfferDetailPresenterImpl presenter;

    @Before
    public void setup(){
        presenter.setView(offerDetailview);
        Whitebox.setInternalState(presenter, "ninjasUtil", ninjasUtil);

    }

    @Test
    public void shouldUpdateDetailScreenWhenFetchingDetailOfferData(){
        String fakeUrl = "http://getninjas.test.br";

        DetailResponse offersResponse = new DetailResponse();
        Mockito.when(offersRepository.getOfferDetail(fakeUrl)).thenReturn(Observable.just(offersResponse));

        presenter.onDetailViewCreated(fakeUrl);

        Mockito.verify(offerDetailview).fillDetailScreen(offersResponse);
        Mockito.verify(offerDetailview).hideProgressBar();

    }

    @Test
    public void shouldShowErrorMessageWhenRequestFail(){
        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(offersRepository.getOfferDetail(fakeUrl)).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        presenter.onDetailViewCreated(fakeUrl);

        Mockito.verify(offerDetailview).onDetailRequestError();
        Mockito.verify(offerDetailview).hideProgressBar();
    }

    @Test
    public void shouldShowInfoValueWithoutComma(){
        List<String> infoValues = new ArrayList<>();
        List<Info> infos = new ArrayList<>();
        infoValues.add("Coffee-break");
        Info eventInfo = new Info("Evento", infoValues);
        Info showInfo = new Info("Show", infoValues);

        infos.add(eventInfo);
        infos.add(showInfo);

        presenter.processInfoList(infos);

        Mockito.verify(offerDetailview).fillInfo("Evento", "Coffee-break");
        Mockito.verify(offerDetailview).fillInfo("Show", "Coffee-break");

    }

    @Test
    public void shouldShowInfoValueSeparatedByComma(){
        List<String> infoValues = new ArrayList<>();
        List<Info> infos = new ArrayList<>();
        infoValues.add("Email");
        infoValues.add("Whatsapp");
        infoValues.add("Phone");

        Info eventInfo = new Info("Evento", infoValues);
        Info showInfo = new Info("Show", infoValues);

        infos.add(eventInfo);
        infos.add(showInfo);
        Mockito.when(ninjasUtil.addCommaToStr(infoValues)).thenReturn("Email, Whatsapp, Phone, ");
        presenter.processInfoList(infos);

        Mockito.verify(offerDetailview).fillInfo("Evento", "Email, Whatsapp, Phone");
        Mockito.verify(offerDetailview).fillInfo("Show", "Email, Whatsapp, Phone");


    }
}
