package br.com.ninjastestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.core.model.RootLinks;
import br.com.ninjastestapp.lead.model.LeadsResponse;
import br.com.ninjastestapp.lead.presenter.impl.LeadPresenterImpl;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import br.com.ninjastestapp.lead.view.LeadView;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class LeadPresenterImplTest {

    @Mock
    LeadsRepository leadsRepository;

    @Mock
    LeadView leadView;

    @InjectMocks
    LeadPresenterImpl leadsPresenter;

    @Mock
    LeadsResponse leadsResponse;

    @Mock
    RootLinks rootLinks;

    @Mock
    Link link;

    @Before
    public void setup(){
        leadsPresenter.setView(leadView);
    }

    @Test
    public void shouldUpdateLeadsScreenWhenFetchingLeadData(){
        Mockito.when(leadsRepository.getLeads()).thenReturn(Observable.just(leadsResponse));

        leadsPresenter.onViewCreated();

        Mockito.verify(leadView).onLeadsLoaded(leadsResponse);

    }

    @Test
    public void shouldHideProgressBarWhenRequestEnds(){
        Mockito.when(leadsRepository.getLeads()).thenReturn(Observable.just(leadsResponse));

        leadsPresenter.onViewCreated();

        Mockito.verify(leadView).hideProgressBar();

    }

    @Test
    public void shouldShowErrorMessageWhenRequestFail(){
        Mockito.when(leadsRepository.getLeads()).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        leadsPresenter.onViewCreated();

        Mockito.verify(leadView).showLeadRequestFail();
        Mockito.verify(leadView).hideProgressBar();
    }

    @Test
    public void shouldUpdateScreenWhenPullToRefreshIsFinished() {

        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(leadsResponse.getLinks()).thenReturn(rootLinks);
        Mockito.when(leadsResponse.getLinks().getUrl()).thenReturn(link);
        Mockito.when(leadsResponse.getLinks().getUrl().getHref()).thenReturn(fakeUrl);
        Mockito.when(leadsRepository.updateLeads(fakeUrl)).thenReturn(Observable.just(leadsResponse));

        leadsPresenter.updateList(leadsResponse);

        Mockito.verify(leadView).onUpdateFinished(leadsResponse);
        Mockito.verify(leadView).hideProgressBar();

    }

    @Test
    public void shouldShowMessageWhenPullToRefreshIsFails() {

        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(leadsResponse.getLinks()).thenReturn(rootLinks);
        Mockito.when(leadsResponse.getLinks().getUrl()).thenReturn(link);
        Mockito.when(leadsResponse.getLinks().getUrl().getHref()).thenReturn(fakeUrl);
        Mockito.when(leadsRepository.updateLeads(fakeUrl)).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        leadsPresenter.updateList(leadsResponse);

        Mockito.verify(leadView).showLeadRequestFail();
        Mockito.verify(leadView).hideProgressBar();

    }

}
