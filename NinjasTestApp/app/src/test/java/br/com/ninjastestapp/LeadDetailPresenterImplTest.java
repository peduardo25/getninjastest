package br.com.ninjastestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.DetailResponse;
import br.com.ninjastestapp.core.model.Info;
import br.com.ninjastestapp.core.utils.NinjasUtil;
import br.com.ninjastestapp.lead.presenter.impl.LeadDetailPresenterImpl;
import br.com.ninjastestapp.lead.repository.LeadsRepository;
import br.com.ninjastestapp.lead.view.LeadDetailView;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class LeadDetailPresenterImplTest {

    @Mock
    LeadsRepository leadsRepository;

    @Mock
    LeadDetailView leadDetailView;

    @Mock
    NinjasUtil ninjasUtil;

    @InjectMocks
    LeadDetailPresenterImpl presenter;

    @Before
    public void setup(){
        presenter.setView(leadDetailView);
        Whitebox.setInternalState(presenter, "ninjasUtil", ninjasUtil);

    }

    @Test
    public void shouldUpdateDetailScreenWhenFetchingDetailLeadData(){
        String fakeUrl = "http://getninjas.test.br";

        DetailResponse leadResponse = new DetailResponse();
        Mockito.when(leadsRepository.getLeadDetail(fakeUrl)).thenReturn(Observable.just(leadResponse));

        presenter.onDetailViewCreated(fakeUrl);

        Mockito.verify(leadDetailView).fillDetailScreen(leadResponse);
        Mockito.verify(leadDetailView).hideProgressBar();

    }

    @Test
    public void shouldShowErrorMessageWhenRequestFail(){
        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(leadsRepository.getLeadDetail(fakeUrl)).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        presenter.onDetailViewCreated(fakeUrl);

        Mockito.verify(leadDetailView).showDetailRequestError();
        Mockito.verify(leadDetailView).hideProgressBar();
    }

    @Test
    public void shouldShowInfoValueWithoutComma(){
        List<String> infoValues = new ArrayList<>();
        List<Info> infos = new ArrayList<>();
        infoValues.add("Coffee-break");
        Info eventInfo = new Info("Evento", infoValues);
        Info showInfo = new Info("Show", infoValues);

        infos.add(eventInfo);
        infos.add(showInfo);

        presenter.processInfoList(infos);

        Mockito.verify(leadDetailView).fillLeadInfo("Evento", "Coffee-break");
        Mockito.verify(leadDetailView).fillLeadInfo("Show", "Coffee-break");

    }

    @Test
    public void shouldShowInfoValueSeparatedByComma(){
        List<String> infoValues = new ArrayList<>();
        List<Info> infos = new ArrayList<>();
        infoValues.add("Email");
        infoValues.add("Whatsapp");
        infoValues.add("Phone");

        Info eventInfo = new Info("Evento", infoValues);
        Info showInfo = new Info("Show", infoValues);

        infos.add(eventInfo);
        infos.add(showInfo);
        Mockito.when(ninjasUtil.addCommaToStr(infoValues)).thenReturn("Email, Whatsapp, Phone, ");
        presenter.processInfoList(infos);

        Mockito.verify(leadDetailView).fillLeadInfo("Evento", "Email, Whatsapp, Phone");
        Mockito.verify(leadDetailView).fillLeadInfo("Show", "Email, Whatsapp, Phone");


    }
}
