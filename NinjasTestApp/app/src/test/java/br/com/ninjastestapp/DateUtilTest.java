package br.com.ninjastestapp;


import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.ninjastestapp.core.utils.DateUtil;

@RunWith(MockitoJUnitRunner.class)
public class DateUtilTest {

    @InjectMocks
    DateUtil dateUtil;

    @Test
    public void shouldReturnDateWithOfferFormat(){

        String createdAt = "2016-03-04T14:47:05.000+00:00";
        Assert.assertEquals(dateUtil.convertDateToOfferFormat(createdAt), "4 de mar");
    }

    @Test
    public void shouldReturnSameValueWhenDateFormatIsNotParseAble(){

        String createdAt = "2016-03-04";
        Assert.assertEquals(dateUtil.convertDateToOfferFormat(createdAt), createdAt);

    }
}
