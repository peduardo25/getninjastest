package br.com.ninjastestapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.ninjastestapp.core.model.Link;
import br.com.ninjastestapp.core.model.RootLinks;
import br.com.ninjastestapp.offer.model.OffersResponse;
import br.com.ninjastestapp.offer.presenter.impl.OffersPresenterImpl;
import br.com.ninjastestapp.offer.repository.OffersRepository;
import br.com.ninjastestapp.offer.view.OffersView;
import rx.Observable;

@RunWith(MockitoJUnitRunner.class)
public class OfferPresenterImplTest {

    @Mock
    OffersRepository offersRepository;

    @Mock
    OffersView offersView;

    @InjectMocks
    OffersPresenterImpl offersPresenter;

    @Mock
    OffersResponse offersResponse;

    @Mock
    RootLinks rootLinks;

    @Mock
    Link link;

    @Before
    public void setup(){
        offersPresenter.setView(offersView);
    }

    @Test
    public void shouldUpdateOfferListScreenWhenFetchingOfferData(){
        Mockito.when(offersRepository.getOffers()).thenReturn(Observable.just(offersResponse));

        offersPresenter.onViewCreated();

        Mockito.verify(offersView).updateOffersListScreen(offersResponse);

    }

    @Test
    public void shouldHideProgressBarWhenRequestEnds(){
        Mockito.when(offersRepository.getOffers()).thenReturn(Observable.just(offersResponse));

        offersPresenter.onViewCreated();

        Mockito.verify(offersView).hideProgressBar();

    }

    @Test
    public void shouldShowErrorMessageWhenRequestFail(){
        Mockito.when(offersRepository.getOffers()).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        offersPresenter.onViewCreated();

        Mockito.verify(offersView).showRequestErrorMsg();
        Mockito.verify(offersView).hideProgressBar();
    }

    @Test
    public void shouldUpdateScreenWhenPullToRefreshIsFinished() {

        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(offersResponse.getLinks()).thenReturn(rootLinks);
        Mockito.when(offersResponse.getLinks().getUrl()).thenReturn(link);
        Mockito.when(offersResponse.getLinks().getUrl().getHref()).thenReturn(fakeUrl);
        Mockito.when(offersRepository.updateOffers(fakeUrl)).thenReturn(Observable.just(offersResponse));

        offersPresenter.updateList(offersResponse);

        Mockito.verify(offersView).onUpdateFinished(offersResponse);
        Mockito.verify(offersView).hideProgressBar();

    }

    @Test
    public void shouldShowMessageWhenPullToRefreshIsFails() {

        String fakeUrl = "http://getninjas.test.br";
        Mockito.when(offersResponse.getLinks()).thenReturn(rootLinks);
        Mockito.when(offersResponse.getLinks().getUrl()).thenReturn(link);
        Mockito.when(offersResponse.getLinks().getUrl().getHref()).thenReturn(fakeUrl);
        Mockito.when(offersRepository.updateOffers(fakeUrl)).thenReturn(Observable.create(subscriber ->
                subscriber.onError(new RuntimeException())));

        offersPresenter.updateList(offersResponse);

        Mockito.verify(offersView).showRequestErrorMsg();
        Mockito.verify(offersView).hideProgressBar();

    }


}
