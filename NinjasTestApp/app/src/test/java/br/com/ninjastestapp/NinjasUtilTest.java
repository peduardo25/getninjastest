package br.com.ninjastestapp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import br.com.ninjastestapp.core.model.Phone;
import br.com.ninjastestapp.core.utils.NinjasUtil;

@RunWith(MockitoJUnitRunner.class)
public class NinjasUtilTest {

    @InjectMocks
    NinjasUtil ninjasUtil;

    @Test
    public void shouldConcatPhoneNumbersTest(){

        List<Phone> phones = new ArrayList<>();
        Phone phone1 = new Phone();
        phone1.setNumber("111111");

        Phone phone2 = new Phone();
        phone2.setNumber("222222");
        phones.add(phone1);
        phones.add(phone2);

        Assert.assertEquals(ninjasUtil.concatPhones(phones), "111111|222222");


    }

    @Test
    public void shouldReturnDistanceInKM(){
        Assert.assertEquals(ninjasUtil.calcDistanceInKm(10000), "1.0");

    }

    @Test
    public void shouldSplitValueByComma(){
        List<String> infoValues = new ArrayList<>();
        infoValues.add("Email");
        infoValues.add("Whatsapp");
        infoValues.add("Phone");

        Assert.assertEquals(ninjasUtil.addCommaToStr(infoValues), "Email, Whatsapp, Phone, ");

    }
}
